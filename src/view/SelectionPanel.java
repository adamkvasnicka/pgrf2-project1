package view;

import model.*;
import transforms.Mat4;
import transforms.Mat4OrthoRH;
import transforms.Mat4PerspRH;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

class SelectionPanel extends JPanel {
    private static final Mat4 MAT_ORTHO = new Mat4OrthoRH(6, 6, 0.001, 100);
    private static final Mat4 MAT_PERSP = new Mat4PerspRH(Math.PI / 4, 1, 0.001, 100);

    private Canvas parent;
    private SolidBase selectedObject;
    private boolean shouldFill, shouldDrawCube, shouldDrawTriangle, shouldDrawMesh;
    private Mat4 projectionMat;

    SelectionPanel(int width, int height, Canvas parent) {
        this.parent = parent;
        setSize(new Dimension(width, height));
        setFocusable(false);
        setLayout(new GridLayout(0, 1));
        setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        renderCheckBoxes();
        renderProjectionGroup();
        renderFillGroup();

        JButton hint = new JButton("Napoveda");
        hint.addActionListener(e -> JOptionPane.showMessageDialog(this.getTopLevelAncestor(),
                "Pro Pohyb - W,S,A,D" +
                        "\nPro Rozhlizeni - Leve tlacitko mysi" +
                        "\nPro Rotaci Objektu - Prave tlacitko mysi" +
                        "\nPro Zoom in/Zoom out (pri perspektivni projekci) - Kolečko mysi" +
                        "\nPro Posun Objektu - Šipky"
                , "Napoveda", JOptionPane.INFORMATION_MESSAGE));
        add(hint);
    }

    private void renderCheckBoxes() {
        shouldDrawCube = true;
        JCheckBox cube = new JCheckBox("Kostka", true);
        shouldDrawTriangle = true;
        JCheckBox triangle = new JCheckBox("Trojuhelnik", true);
        shouldDrawMesh = false;
        JCheckBox mesh = new JCheckBox("Bikubicka", false);

        mesh.addItemListener(e -> {
            shouldDrawMesh = e.getStateChange() == ItemEvent.SELECTED;
            draw();
        });
        cube.addItemListener(e -> {
            shouldDrawCube = e.getStateChange() == ItemEvent.SELECTED;
            draw();
        });
        triangle.addItemListener(e -> {
            shouldDrawTriangle = e.getStateChange() == ItemEvent.SELECTED;
            draw();
        });

        add(new JLabel("What should draw"));
        add(cube);
        add(triangle);
        add(mesh);
    }

    private void renderFillGroup() {
        ButtonGroup renderGroup = new ButtonGroup();
        shouldFill = false;

        JRadioButton frame = new JRadioButton("Dratovy", true);
        frame.setFocusable(false);
        JRadioButton fill = new JRadioButton("Vyplneny");
        fill.setFocusable(false);
        renderGroup.add(frame);
        renderGroup.add(fill);

        frame.addActionListener((e) -> {
            shouldFill = false;
            draw();
        });

        fill.addActionListener((e) -> {
            shouldFill = true;
            draw();
        });

        add(new JLabel("Mod"));
        add(frame);
        add(fill);
    }

    private void renderProjectionGroup() {
        ButtonGroup projectionGroup = new ButtonGroup();
        projectionMat = MAT_ORTHO;

        JRadioButton projectionOtrho = new JRadioButton("Orthogonalni", true);
        JRadioButton projectionPers = new JRadioButton("Perspektivni");
        projectionOtrho.setFocusable(false);
        projectionPers.setFocusable(false);
        projectionGroup.add(projectionPers);
        projectionGroup.add(projectionOtrho);

        projectionOtrho.addActionListener((e) -> {
            projectionMat = MAT_ORTHO;
            draw();
        });
        projectionPers.addActionListener((e) -> {
            projectionMat = MAT_PERSP;
            draw();
        });

        add(new JLabel("Projekce"));
        add(projectionOtrho);
        add(projectionPers);
    }

    boolean shouldDrawCube() {
        return shouldDrawCube;
    }

    boolean shouldDrawMesh() {
        return shouldDrawMesh;
    }

    boolean shouldDrawTriangle() {
        return shouldDrawTriangle;
    }

    boolean isShouldFill() {
        return shouldFill;
    }

    private void draw() {
        parent.draw();
    }

    Mat4 getProjectionMat() {
        return projectionMat;
    }
}
