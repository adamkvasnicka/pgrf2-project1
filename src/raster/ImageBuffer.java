package raster;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Optional;

public class ImageBuffer implements Zbuffer<Integer> {

	private BufferedImage img;

	public ImageBuffer(BufferedImage img) {
		this.img = img;
	}

	@Override
	public Optional<Integer> getPixel(int x, int y) {
		if (isInside(x, y)) {
			return Optional.of(new Integer(img.getRGB(x, y)));
		}
		return Optional.empty();
	}

	@Override
	public void setPixel(int x, int y, Integer type) {
		if (isInside(x, y)) {
			img.setRGB(x, y, type.intValue());
		}
	}

	@Override
	public int getWidth() {
		return img.getWidth();
	}

	@Override
	public int getHeight() {
		return img.getHeight();
	}

	private boolean isInside(int x, int y) {
		return x >= 0 && y >= 0 && x < img.getWidth() && y < img.getHeight();
	}

	@Override
	public void clear(Integer type) {
		java.awt.Graphics g = img.getGraphics();
		g.setColor(new Color(type.intValue()));
		g.fillRect(0, 0, img.getWidth(), img.getHeight());
	}

}
