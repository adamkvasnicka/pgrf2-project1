package raster;

import java.util.Optional;

public interface Zbuffer<PixelType> {

	Optional<PixelType> getPixel(int x, int y);

	void setPixel(int x, int y, PixelType type);
	
	void clear(PixelType type);

	int getWidth();

	int getHeight();

}
