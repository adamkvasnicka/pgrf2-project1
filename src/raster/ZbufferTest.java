package raster;

import java.awt.image.BufferedImage;
import java.util.Optional;

import transforms.Col;

public class ZbufferTest {
    private DepthBuffer depthBuffer;
    private ImageBuffer imageBuffer;
    private BufferedImage img;

    public ZbufferTest(BufferedImage img) {
        this.img = img;
        this.depthBuffer = new DepthBuffer(img);
        this.imageBuffer = new ImageBuffer(img);
    }

    public void zTest(int x, int y, double z, Col color) {
        Optional<Double> zValue = depthBuffer.getPixel(x, y);
        if (zValue.isPresent() && z >= 0 && z < zValue.get()) {
            depthBuffer.setPixel(x, y, z);
            imageBuffer.setPixel(x, y, color.getRGB());
        }
    }

    public void clear() {
        depthBuffer.clear(1.0);
    }

    public BufferedImage getImg() {
        return img;
    }

    public int getWidth() {
        return img.getWidth();
    }

    public int getHeight() {
        return img.getHeight();
    }

}
