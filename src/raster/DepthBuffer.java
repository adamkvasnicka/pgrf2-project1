package raster;

import java.awt.image.BufferedImage;
import java.util.Optional;

public class DepthBuffer implements Zbuffer<Double> {
    private double[][] depth;
    private int width;
    private int height;

    public DepthBuffer(int width, int height) {
        // [rows - osa Y][columns - osa X] but because the way we access them we will switch them
        this.depth = new double[width][height];
        this.width = width;
        this.height = height;
        this.clear(1.0);
    }

    public DepthBuffer(BufferedImage img) {
        this(img.getWidth(), img.getHeight());
    }

    @Override
    public Optional<Double> getPixel(int x, int y) {
        if (isInside(x, y)) {
            return Optional.of(depth[x][y]);
        }
        return Optional.empty();
    }

    @Override
    public void setPixel(int x, int y, Double type) {
        if (isInside(x, y)) {
            depth[x][y] = type;
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    private boolean isInside(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    @Override
    public void clear(Double type) {
        for (int i = 0; i < width; i++) {
            for (int l = 0; l < height; l++) {
                depth[i][l] = type;
            }
        }
    }

}
