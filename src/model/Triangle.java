package model;

import transforms.Col;
import transforms.Point3D;
import transforms.Vec2D;
import transforms.Vec3D;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Trida reprezentujici Ctyrsten
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class Triangle extends SolidBase {
    public Triangle() {
        transform = true;
        Integer[] indices = new Integer[]{
                0, 1, 3,
                0, 1, 2,
                3, 2, 1,
                3, 2, 0
        };
        indexBuffer = new ArrayList<>(Arrays.asList(indices));

        parts = new ArrayList<>();
        parts.add(new Part(Part.Type.TRIANGLE, 0, 12));

        Vertex v1 = new Vertex(new Point3D(-0.5, 0, -0.5), new Col(0x000000), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1);
        Vertex v2 = new Vertex(new Point3D(0.5, 0, -0.5), new Col(0xff0000), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1);
        Vertex v3 = new Vertex(new Point3D(0, -0.5, 0.5), new Col(0x00ff00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1);
        Vertex v4 = new Vertex(new Point3D(0, 0.5, 0.5), new Col(0x0000ff), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1);

        vertexBuffer = new ArrayList<>();
        vertexBuffer.add(v1);
        vertexBuffer.add(v2);
        vertexBuffer.add(v3);
        vertexBuffer.add(v4);

    }
}
