package model;

import transforms.Col;
import transforms.Point3D;
import transforms.Vec2D;
import transforms.Vec3D;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Trida reprezentujici Krychli
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class Cube extends SolidBase {
    public Cube() {
        transform = true;
        vertexBuffer = new ArrayList<>();
        parts = new ArrayList<>();
        parts.add(new Part(Part.Type.TRIANGLE, 0, 36));

        Integer[] indices = {
                // Bottom
                0, 1, 3,
                1, 2, 3,
                // Top
                4, 5, 7,
                5, 6, 7,
                // Left
                0, 1, 5,
                0, 4, 5,
                // Front
                5, 6, 2,
                2, 1, 5,
                // Right
                2, 6, 7,
                2, 3, 7,
                // Back
                0, 4, 7,
                0, 3, 7
        };
        indexBuffer = new ArrayList<>(Arrays.asList(indices));
        Vertex v1 = new Vertex(new Point3D(-0.25, -0.25, -0.25), new Col(0xff0000), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 0
        Vertex v2 = new Vertex(new Point3D(-0.25, 0.25, -0.25), new Col(0xffff00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 1
        Vertex v3 = new Vertex(new Point3D(0.25, 0.25, -0.25), new Col(0xff00ff), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 2
        Vertex v4 = new Vertex(new Point3D(0.25, -0.25, -0.25), new Col(0xff00ff), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 3
        Vertex v5 = new Vertex(new Point3D(-0.25, -0.25, 0.25), new Col(0x00ff00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 4
        Vertex v6 = new Vertex(new Point3D(-0.25, 0.25, 0.25), new Col(0x0000ff), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 5
        Vertex v7 = new Vertex(new Point3D(0.25, 0.25, 0.25), new Col(0xff0f00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 6
        Vertex v8 = new Vertex(new Point3D(0.25, -0.25, 0.25), new Col(0xff00f0), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1); // 7

        vertexBuffer.add(v1); // 0
        vertexBuffer.add(v2); // 1
        vertexBuffer.add(v3); // 2
        vertexBuffer.add(v4); // 3
        vertexBuffer.add(v5); // 4
        vertexBuffer.add(v6); // 5
        vertexBuffer.add(v7); // 6
        vertexBuffer.add(v8); // 7
    }
}
