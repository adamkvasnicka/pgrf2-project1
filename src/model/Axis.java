package model;

import transforms.Col;
import transforms.Point3D;
import transforms.Vec2D;
import transforms.Vec3D;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Trida reprezentujici Osy
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class Axis extends SolidBase {

    public Axis() {
        transform = false;

        Integer[] ints = new Integer[]{0, 1, 0, 2, 0, 3};

        indexBuffer = new ArrayList<>(Arrays.asList(ints));

        parts = new ArrayList<>();
        parts.add(new Part(Part.Type.LINES, 0, 6));

        vertexBuffer = new ArrayList<>();
        vertexBuffer.add(new Vertex(new Point3D(0, 0, 0), new Col(0x00FF00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1));
        vertexBuffer.add(new Vertex(new Point3D(0, 0, 1), new Col(0xFF0000), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1));
        vertexBuffer.add(new Vertex(new Point3D(0, 1, 0), new Col(0x00FF00), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1));
        vertexBuffer.add(new Vertex(new Point3D(1, 0, 0), new Col(0x0000FF), new Vec3D(0, 1, 1), new Vec2D(0, 0), 1));
    }

}
