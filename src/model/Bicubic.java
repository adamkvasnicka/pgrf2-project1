package model;

import transforms.*;

import java.util.ArrayList;
import java.util.Random;

public class Bicubic extends SolidBase {

    public Bicubic() {
        transform = true;
        int numberOfPoints = 8;

        Point3D p11 = new Point3D(1, 1, 2);
        Point3D p12 = new Point3D(3, -1, 1);
        Point3D p13 = new Point3D(0, 2, 1);
        Point3D p14 = new Point3D(2, -1, -1);

        Point3D p21 = new Point3D(2, 1, 0);
        Point3D p22 = new Point3D(1, 2, 1);
        Point3D p23 = new Point3D(0, 0, 2);
        Point3D p24 = new Point3D(1, 2, 1);

        Point3D p31 = new Point3D(0, 2, 2);
        Point3D p32 = new Point3D(-1, 1, 1);
        Point3D p33 = new Point3D(0, 2, 2);
        Point3D p34 = new Point3D(1, 0, -1);

        Point3D p41 = new Point3D(1, 1, 0);
        Point3D p42 = new Point3D(2, 3, 0);
        Point3D p43 = new Point3D(1, 1, -1);
        Point3D p44 = new Point3D(0, 1, 1);

        transforms.Bicubic bicubic = new transforms.Bicubic(Cubic.BEZIER, p11, p12, p13, p14, p21, p22, p23, p24, p31, p32, p33, p34, p41, p42, p43, p44);

        indexBuffer = new ArrayList<>();
        vertexBuffer = new ArrayList<>();

        Random randomCol = new Random();

        // Vertexy
        for (int i = 0; i < numberOfPoints; i++) {
            for (int j = 0; j < numberOfPoints; j++) {
                // Kazdy Vrchol bude mit nahodnou barvu svetleho odstinu
                vertexBuffer.add(
                        new Vertex(bicubic.compute((double) i / numberOfPoints, (double) j / numberOfPoints),
                                new Col(randomCol.nextFloat() / 2f + 0.5,randomCol.nextFloat() / 2f + 0.5,randomCol.nextFloat() / 2f + 0.5),
                                new Vec3D(0, 1, 1),
                                new Vec2D(0 ,0)));
            }
        }

        // Index vertexu
        for (int i = 0; i < vertexBuffer.size() - numberOfPoints; i+= numberOfPoints) {
            for (int j = 0; j < numberOfPoints - 1; j++) {
                indexBuffer.add(j + i);
                indexBuffer.add(j + i + 1);
                indexBuffer.add(j + i + numberOfPoints);

                indexBuffer.add(j + i + 1);
                indexBuffer.add(j + i + 1 + numberOfPoints);
                indexBuffer.add(j + i + numberOfPoints);
            }
        }

        parts = new ArrayList<>();
        parts.add(new Part(Part.Type.TRIANGLE, 0, indexBuffer.size()));
    }

}
