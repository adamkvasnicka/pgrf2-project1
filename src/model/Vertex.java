package model;

import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec2D;
import transforms.Vec3D;

import java.util.Optional;

/**
 * Trida reprezentujici Vrchol
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class Vertex {
    private final Point3D position;
    private final Col color;
    private final Vec3D normal;
    private final Vec2D texUV;
    private double one;

    public Vertex(Point3D position, Col color, Vec3D normal, Vec2D texUV) {
        this(position, color, normal, texUV, 1.0);
    }

    public Vertex(Point3D position, Col color, Vec3D normal, Vec2D texUV, double one) {
        super();
        this.position = position;
        this.color = color;
        this.normal = normal;
        this.texUV = texUV;
        this.one = one;
    }

    public Point3D getPosition() {
        return position;
    }

    public int getColor() {
        return color.getRGB();
    }

    public Col getCol() {
        return color;
    }

    public Vec3D getNormal() {
        return normal;
    }

    public Vec2D getTexUV() {
        return texUV;
    }

    public double getOne() {
        return one;
    }

    public Vec3D getDehomogVec() {
        return new Vec3D(this.position.getX(),this.position.getY(),this.position.getZ());
    }

    public Vertex mul(Mat4 mat) {
        return new Vertex(this.position.mul(mat), this.color, this.normal, this.texUV, this.one);
    }

    public Vertex mul(double t) {
        return new Vertex(position.mul(t), color.mul(t), normal.mul(t), texUV.mul(t), one * t);
    }

    // Korektní s ohledem na to že někdo by nám mohl hodit null
    public Vertex add(Vertex v) {
        return new Vertex(
                position == null || v.getPosition() == null ? null : position.add(v.getPosition()),
                color == null || v.getCol() == null ? null : color.add(v.getCol()),
                normal == null || v.getNormal() == null ? null : normal.add(v.getNormal()),
                texUV == null || v.getTexUV() == null ? null : texUV.add(v.getTexUV()),
                one + v.getOne());
    }

    // Korektní perspektivní interpolace
    public Optional<Vertex> dehomog() {
        if (position.getW() == 0) {
            return Optional.empty();
        } else {
            return Optional.of(
                    new Vertex(
                            position.mul(1 / position.getW()),
                            color,
                            normal.mul(1 / position.getW()),
                            texUV.mul(1 / position.getW()),
                            one * (1 / position.getW()) // maybe not like this
                    )
            );
        }
    }
}
