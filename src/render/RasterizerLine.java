package render;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.Vertex;
import transforms.Vec3D;

/**
 * Trida slouzici k rasterizaci na platno
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class RasterizerLine {
    private BufferedImage img;

    public RasterizerLine(BufferedImage img) {
        this.img = img;
    }

    public void rasterize(Vertex vertexA, Vertex vertexB) {

        // Prisne rychle orrezani
        if (Util.fastCut(vertexA.getPosition()) || Util.fastCut(vertexB.getPosition())) {
            return;
        }
        // Kontrola pred dehomogenizaci
        if (!vertexA.getPosition().dehomog().isPresent() || !vertexB.getPosition().dehomog().isPresent()) {
            return;
        }
        // Dehomogenizace
        Vec3D vecA = vertexA.getPosition().dehomog().get();
        Vec3D vecB = vertexB.getPosition().dehomog().get();
        // Transformace do okna
        Vec3D vecAFinal = Util.viewportTransformation(vecA, img.getWidth(), img.getHeight());
        Vec3D vecBFinal = Util.viewportTransformation(vecB, img.getWidth(), img.getHeight());

        draw(vecAFinal, vecBFinal, vertexB.getColor());
    }

    /**
     * Metoda pro resterizaci hrany na platno
     *
     * @param v1
     * @param v2
     * @param color
     */
    public void draw(Vec3D v1, Vec3D v2, int color) {
        int x1 = (int) v1.getX();
        int y1 = (int) v1.getY();

        int x2 = (int) v2.getX();
        int y2 = (int) v2.getY();

        Graphics g = img.getGraphics();
        g.setColor(new Color(color));
        g.drawLine(x1, y1, x2, y2);
    }

}
