package render;

import java.awt.*;
import java.util.function.Function;

import model.Vertex;
import raster.ZbufferTest;
import transforms.*;

public class RasterizerTriangle {
    private ZbufferTest zTest;
    private Function<Vertex, Col> shader;

    public RasterizerTriangle(ZbufferTest zTest, Function<Vertex, Col> shader) {
        super();
        this.zTest = zTest;
        this.shader = shader;
    }

    public void rasterize(Vertex vA, Vertex vB, Vertex vC, boolean shouldFill) {

        // Ne tak prisne orezani
        if(Util.fastCut(vA.getPosition()) && Util.fastCut(vB.getPosition()) && Util.fastCut(vC.getPosition())) return;

        if(vA.getPosition().getZ() < vB.getPosition().getZ()) {
            Vertex temp = vA;
            vA = vB;
            vB = temp;
        }
        if(vB.getPosition().getZ() < vC.getPosition().getZ()) {
            Vertex temp = vB;
            vB = vC;
            vC = temp;
        }
        if(vA.getPosition().getZ() < vB.getPosition().getZ()) {
            Vertex temp = vA;
            vA = vB;
            vB = temp;
        }

        if(vC.getPosition().getZ() > 0) {
            draw(vA,vB,vC,shouldFill);
            return;
        }

        if(vB.getPosition().getZ() > 0) {
            double t1 = (0 - vC.getPosition().getZ()) / (vB.getPosition().getZ() - vC.getPosition().getZ());
            double t2 = (0 - vC.getPosition().getZ()) / (vA.getPosition().getZ() - vC.getPosition().getZ());
            Vertex vD = vC.mul(1 - t1).add(vB.mul(t1));
            Vertex vE = vC.mul(1-t2).add(vA.mul(t2));
            draw(vA,vB,vD,shouldFill);
            draw(vA,vD,vE,shouldFill);
            return;
        }

        if(vA.getPosition().getZ() > 0) {
            double t1 = (0 - vC.getPosition().getZ()) / (vA.getPosition().getZ() - vC.getPosition().getZ());
            double t2 = (0 - vB.getPosition().getZ()) / (vA.getPosition().getZ() - vB.getPosition().getZ());
            Vertex vD = vC.mul(1 - t1).add(vA.mul(t1));
            Vertex vE = vB.mul(1-t2).add(vA.mul(t2));
            draw(vA,vD,vE,shouldFill);
        }
    }


    private void draw(Vertex v1,Vertex v2,Vertex v3,boolean shouldFill) {

        if (!v1.dehomog().isPresent() || !v2.dehomog().isPresent() || !v3.dehomog().isPresent())
            return;

        // Korektní dehomogenizace každé části vertexu
        Vertex vA = v1.dehomog().get();
        Vertex vB = v2.dehomog().get();
        Vertex vC = v3.dehomog().get();

        // Transformace do viewportu
        Vec3D a = Util.viewportTransformation(vA.getDehomogVec(),zTest.getWidth(),zTest.getHeight());
        Vec3D b =  Util.viewportTransformation(vB.getDehomogVec(),zTest.getWidth(),zTest.getHeight());
        Vec3D c =  Util.viewportTransformation(vC.getDehomogVec(),zTest.getWidth(),zTest.getHeight());


        // Setřízení všeho podle Y od nejmenšího po největší A < B < C
        if (a.getY() > b.getY()) {
            Vec3D temp = a;
            a = b;
            b = temp;

            Vertex vTemp = vA;
            vA = vB;
            vB = vTemp;
        }
        if (b.getY() > c.getY()) {
            Vec3D temp = b;
            b = c;
            c = temp;

            Vertex vTemp = vB;
            vB = vC;
            vC = vTemp;
        }
        if (a.getY() > b.getY()) {
            Vec3D temp = a;
            a = b;
            b = temp;

            Vertex vTemp = vA;
            vA = vB;
            vB = vTemp;
        }

        // TODO Ořez

        if (!shouldFill) {
            dontFill(a, b, c);
            return;
        }

        // It goes from up to bottom A > B > C

        // Upper part of triangle
        for (int y = (int) (a.getY() + 1); y < b.getY(); y++) {

            // TODO test meze obrazovky

            double t1 = (y - a.getY()) / (b.getY() - a.getY());
            double t2 = (y - a.getY()) / (c.getY() - a.getY());

            Vec3D vAB = a.mul(1 - t1).add(b.mul(t1)); //x1
            Vec3D vAC = a.mul(1 - t2).add(c.mul(t2)); //x2

            Vertex vertexAB = vA.mul(1 - t1).add(vB.mul(t1));
            Vertex vertexAC = vA.mul(1 - t2).add(vC.mul(t2));

            // Setřízení podle X
            if (vAB.getX() > vAC.getX()) {
                Vec3D temp = vAB;
                vAB = vAC;
                vAC = temp;

                Vertex vTemp = vertexAB;
                vertexAB = vertexAC;
                vertexAC = vTemp;
            }

            for (int x = (int) (vAB.getX() + 1); x < vAC.getX(); x++) {

                double s = (x - vAB.getX()) / (vAC.getX() - vAB.getX()); // For Z interpolation using X
                double z = vAB.getZ() * (1.0 - s) + vAC.getZ() * s;

                Vertex vertexABC = vertexAB.mul(1 - s).add(vertexAC.mul(s)); // Interpolation of color

                zTest.zTest(x, y, z, shader.apply(vertexABC));
            }
        }

        // botttom part of triangle
        for (int y = (int) (b.getY() + 1); y < c.getY(); y++) {

            double t1 = (y - b.getY()) / (c.getY() - b.getY());
            double t2 = (y - a.getY()) / (c.getY() - a.getY());

            Vec3D vBC = b.mul(1 - t1).add(c.mul(t1)); // x1
            Vec3D vAC = a.mul(1 - t2).add(c.mul(t2)); // x2

            Vertex vertexBC = vB.mul(1 - t1).add(vC.mul(t1));
            Vertex vertexAC = vA.mul(1 - t2).add(vC.mul(t2));

            // Setřízení podle X
            if (vBC.getX() > vAC.getX()) {
                Vec3D temp = vBC;
                vBC = vAC;
                vAC = temp;

                Vertex vTemp = vertexBC;
                vertexBC = vertexAC;
                vertexAC = vTemp;
            }

            for (int x = (int) (vBC.getX() + 1); x < vAC.getX(); x++) {

                double s = (x - vBC.getX()) / (vAC.getX() - vBC.getX()); // Interpolační koeficient pro dané x
                double z = vBC.getZ() * (1.0 - s) + vAC.getZ() * s;   // Interpolace Z pro každé X

                Vertex vertexABC = vertexBC.mul(1 - s).add(vertexAC.mul(s));

                zTest.zTest(x, y, z, shader.apply(vertexABC));
            }

        }
    }


    private void dontFill(Vec3D a, Vec3D b, Vec3D c) {
        Graphics g = zTest.getImg().getGraphics();
        g.setColor(new Color(0xffff00));
        g.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
        g.drawLine((int) a.getX(), (int) a.getY(), (int) c.getX(), (int) c.getY());
        g.drawLine((int) b.getX(), (int) b.getY(), (int) c.getX(), (int) c.getY());
    }
}
