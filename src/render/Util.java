package render;

import transforms.Point3D;
import transforms.Vec3D;

import java.awt.image.BufferedImage;

public final class Util {

    /**
     * Metoda pro rychle orezani
     *
     * @param a
     * @return boolean
     */
    static public boolean fastCut(Point3D a) {
        return (-a.getW() >= a.getX() || -a.getW() >= a.getY() || a.getX() >= a.getW() || a.getY() >= a.getW()
                || 0 >= a.getZ() || a.getZ() >= a.getW());
    }

    /**
     * Metoda pro transformaci do viewportu
     *
     * @param vec
     * @param width
     * @param height
     * @return
     */
    static public Vec3D viewportTransformation(Vec3D vec, int width, int height) {
        return vec.mul(new Vec3D(1,-1,1)).add(new Vec3D(1,1,0)).mul(new Vec3D((width - 1) / 2,(height - 1) / 2,1));
    }
}
