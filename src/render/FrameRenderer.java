package render;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import model.*;
import raster.ZbufferTest;
import transforms.*;

/**
 * Trida reprezentujici pohledovy retezec
 *
 * @author Adam Kvasnicka
 * @version 2017
 */
public class FrameRenderer {
    private List<Integer> ib;
    private List<Vertex> vb;
    private Mat4 model, view, projection;
    private RasterizerLine rasterizerLine;
    private RasterizerTriangle rasterizerTriangle;
    private boolean shouldFill;
    private ZbufferTest zTest;

    public FrameRenderer(BufferedImage img) {
        this.zTest = new ZbufferTest(img);
        this.rasterizerLine = new RasterizerLine(img);
        this.shouldFill = false;

        // vertex -> vertex.getCol().mul(1/vertex.getOne()) --- korektni interpolace
        this.rasterizerTriangle = new RasterizerTriangle(zTest, vertex -> vertex.getCol());

        ib = new ArrayList<>();
        vb = new ArrayList<>();
    }

    public void setShouldFill(boolean shouldFill) {
        this.shouldFill = shouldFill;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public void render(SolidBase sb) {

        ib = sb.getIndices();

        if (!sb.getTransform()) {
            model = new Mat4Identity();
        }

        Mat4 matMVP = model.mul(view).mul(projection);

        for (Vertex v : sb.getVertices()) {
            // Transformace vrcholu modelovou, pohledevou a projekcni matici
            vb.add(v.mul(matMVP));
        }

        for (Part part : sb.getParts()) {
            int indexStart = part.getStart();
            int indexCount = part.getCount();
            if (part.getType() == Part.Type.LINES) {
                for (int i = indexStart; i < indexCount; i += 2) {
                    rasterizerLine.rasterize(vb.get(ib.get(i)), vb.get(ib.get(i + 1)));
                }
            } else if (part.getType() == Part.Type.TRIANGLE) {
                for (int i = indexStart; i < indexCount; i += 3) {
                    rasterizerTriangle.rasterize(vb.get(ib.get(i)), vb.get(ib.get(i + 1)), vb.get(ib.get(i + 2)), shouldFill);
                }
            }
        }

        vb = new ArrayList<>();
        ib = new ArrayList<>();
    }

    public void clear() {
        zTest.clear();
    }
}
